**oracle-openGauss外**键**特性设计说明书**

| 所属SIG组: | 无 |
| --- | --- |
| 落入版本: | master |
| 设计人员: | 安磊 |
| 日期: | 2022-03-14 |


**Copyright © 2022 openGauss Community**

您对"本文档"的复制，使用，修改及分发受知识共享(Creative Commons)署名—相同方式共享4.0国际公共许可协议(以下简称"CC BY-SA 4.0")的约束。
为了方便用户理解，您可以通过访问[https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)了解CC BY-SA 4.0的概要 (但不是替代)。
CC BY-SA 4.0的完整协议内容您可以访问如下网址获取：[https://creativecommons.org/licenses/by-sa/4.0/legalcode](https://creativecommons.org/licenses/by-sa/4.0/legalcode)。

**改版记录**

| 日期 | 修订版本 | 修订描述 | 作者 | 审核 |
| --- | --- | --- | --- | --- |
| 2022-03-14 | 1.0 | 初始化 | 安磊 | 安磊 |
| 
 | 
 | 
 | 
 | 
 |


# 1.特性概述

_简述本产品 **/** 特性背景信息，简单概括本方案，对客户的价值、要达成的目的，并说明本文档包含的主要内容、适用范围等。_

## 1.1范围

基于openGauss开源项目onlineMigration（https://gitee.com/opengauss/openGauss-tools-onlineMigration），工具的在线迁移过程中需支持DDL语句迁移，即在线迁移阶段，Oracle侧的DDL语句能够同步到openGauss侧并成功执行

## 1.2特性需求列表

| **类型** | **备注** |
| --- | --- |
| 在线迁移支持**CREATE 外键 ** | https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/constraint.html#GUID-1055EA97-BA6F-4764-A15F-1024FD5B6DFE |
| 在线迁移支持**DROP 外键** | https://docs.oracle.com/cd/B19306_01/server.102/b14200/clauses002.htm |


# 2.需求场景分析

## 2.1特性需求来源与价值概述

**来源于客户需求**

- 项目：openGauss-Oracle在线迁移支持在线迁移DDL语句开发项目
- 功能需求：Oracle在线迁移工具支持在线迁移CREATE/DROP 外键语法

## 2.2特性场景分析


如果您需要从oracle向opengrauss数据库同步获迁移数据时，你的表之间存在外键关系，则需要使用此特性

## 2.3特性影响分析

_外键是数据库常用的用法，在创建业务场景数据模型时，往往存在一些业务场景的关联，例如用户，角色，权限， 可以给角色中授予一批权限，而授予改角色的用户则默认拥有这些权限，那么这些表之间往往存在依赖关系，我们在建立数据模型时，就要使用到外键_

_与其他需求及特性的交互分析：改特性严重依赖创建表数据同步，尤其在_onlineMigration中

_平台差异性分析：使用java实现，可以实现跨平台使用_

_兼容性分析：对以前版本无影响，为改变数据标准结构，只是扩展字段_

_约束及限制：仅支持oracle到_opengrauss

### 2.3.1硬件限制

_在满足原有系统的硬件条件下，该特性并不会额外增加硬件消耗。_

### 2.3.2技术限制

_操作系统：跨平台_

_编程语言：java_


### 2.3.3对License的影响分析

_无新增三方依赖。_

### 2.3.4对系统性能规格的影响分析

无新增系统消耗

### 2.3.5对系统可靠性规格的影响分析

不对原有系统可靠性行造成影响

### 2.3.6对系统兼容性的影响分析

不对原有系统兼容性行造成影响

### 2.3.7与其他重大特性的交互性，冲突性的影响分析

_不改变原有系统交互_

## 2.4同类社区/商用软件实现方案分析

暂无

# 3.特性/功能实现原理(可分解出来多个Use Case)

## 3.1目标

工具的在线迁移阶段，在ORACLE侧执行CREATE/DROP 外键语法能够同步到openGauss侧并成功执行

## 3.2总体方案

- _在debezium测对数据进行分析，将提取的外键放入到table对象，在加入change时间_
- _对change事件进行监控，获取数据，进行分析和结构化处理，发送到kafka_
- onlineMigration从kafka获取到结构化的消息，进行提取和转化，转成opengrauss的sql，执行到数据库

![Image text](https://gitee.com/al229/doc/raw/master/pic/3.2png.png)

# 4.Use Case一实现

## 4.1设计思路

   - 原始sql监控和解析在debezium侧，按照现有开源方案标准格式输出
   - 标准格式数据转化成opengaruss sql 在onlineMigration执行

## 4.2约束条件

标准格式数据转化成opengaruss sql 外键部分依赖与创建表部分

## 4.3详细实现(从用户入口的模块级别或进程级别消息序列图)

 ![Image text](https://gitee.com/al229/doc/raw/master/pic/%E6%95%B0%E6%8D%AE%E5%BA%93%E5%90%8C%E6%AD%A5%E6%B5%81%E7%A8%8B-debezium.jpg)

## 4.4子系统间接口(主要覆盖模块接口定义)


- 创建表的外键输入格式

| **字段** | **是否必须** | 描述 |
| --- | --- | --- |
| pktableSchem | 否 | 父表的Schem |
| pktableName | 否 | 父表的表名 |
| pkColumnName | 否 | 引用的父表列名 |
| fkColumnName | 否 | 外键列名称 |
| fkName | 否 | 外键名称 |


- 新增对象
```json
"foreignKeyColumns":[
   {
    "pkColumnName":"id",
    "fkName":"user_role_ref",
    "fkColumnName":"role_id",
    "pktableName":"role",
    "pktableSchem":"C##DDL_TEST"
  }
]
```
- 全量数据数据格式
```json
{
    "schema":{
        "type":"struct",
        "fields":[
            {
                "type":"struct",
                "fields":[
                    {
                        "type":"string",
                        "optional":false,
                        "field":"version"
                    },
                    {
                        "type":"string",
                        "optional":false,
                        "field":"connector"
                    },
                    {
                        "type":"string",
                        "optional":false,
                        "field":"name"
                    },
                    {
                        "type":"int64",
                        "optional":false,
                        "field":"ts_ms"
                    },
                    {
                        "type":"string",
                        "optional":true,
                        "name":"io.debezium.data.Enum",
                        "version":1,
                        "parameters":{
                            "allowed":"true,last,false,incremental"
                        },
                        "default":"false",
                        "field":"snapshot"
                    },
                    {
                        "type":"string",
                        "optional":false,
                        "field":"db"
                    },
                    {
                        "type":"string",
                        "optional":true,
                        "field":"sequence"
                    },
                    {
                        "type":"string",
                        "optional":false,
                        "field":"schema"
                    },
                    {
                        "type":"string",
                        "optional":false,
                        "field":"table"
                    },
                    {
                        "type":"string",
                        "optional":true,
                        "field":"txId"
                    },
                    {
                        "type":"string",
                        "optional":true,
                        "field":"scn"
                    },
                    {
                        "type":"string",
                        "optional":true,
                        "field":"commit_scn"
                    },
                    {
                        "type":"string",
                        "optional":true,
                        "field":"lcr_position"
                    }
                ],
                "optional":false,
                "name":"io.debezium.connector.oracle.Source",
                "field":"source"
            },
            {
                "type":"string",
                "optional":true,
                "field":"databaseName"
            },
            {
                "type":"string",
                "optional":true,
                "field":"schemaName"
            },
            {
                "type":"string",
                "optional":true,
                "field":"ddl"
            },
            {
                "type":"array",
                "items":{
                    "type":"struct",
                    "fields":[
                        {
                            "type":"string",
                            "optional":false,
                            "field":"type"
                        },
                        {
                            "type":"string",
                            "optional":false,
                            "field":"id"
                        },
                        {
                            "type":"struct",
                            "fields":[
                                {
                                    "type":"string",
                                    "optional":true,
                                    "field":"defaultCharsetName"
                                },
                                {
                                    "type":"array",
                                    "items":{
                                        "type":"string",
                                        "optional":false
                                    },
                                    "optional":true,
                                    "field":"primaryKeyColumnNames"
                                },
                                {
                                    "type":"array",
                                    "items":{
                                        "type":"map",
                                        "keys":{
                                            "type":"string",
                                            "optional":true
                                        },
                                        "values":{
                                            "type":"string",
                                            "optional":true
                                        },
                                        "optional":true
                                    },
                                    "optional":true,
                                    "field":"foreignKeyColumns"
                                },
                                {
                                    "type":"array",
                                    "items":{
                                        "type":"struct",
                                        "fields":[
                                            {
                                                "type":"string",
                                                "optional":false,
                                                "field":"name"
                                            },
                                            {
                                                "type":"int32",
                                                "optional":false,
                                                "field":"jdbcType"
                                            },
                                            {
                                                "type":"int32",
                                                "optional":true,
                                                "field":"nativeType"
                                            },
                                            {
                                                "type":"string",
                                                "optional":false,
                                                "field":"typeName"
                                            },
                                            {
                                                "type":"string",
                                                "optional":true,
                                                "field":"typeExpression"
                                            },
                                            {
                                                "type":"string",
                                                "optional":true,
                                                "field":"charsetName"
                                            },
                                            {
                                                "type":"int32",
                                                "optional":true,
                                                "field":"length"
                                            },
                                            {
                                                "type":"int32",
                                                "optional":true,
                                                "field":"scale"
                                            },
                                            {
                                                "type":"int32",
                                                "optional":false,
                                                "field":"position"
                                            },
                                            {
                                                "type":"boolean",
                                                "optional":true,
                                                "field":"optional"
                                            },
                                            {
                                                "type":"boolean",
                                                "optional":true,
                                                "field":"autoIncremented"
                                            },
                                            {
                                                "type":"boolean",
                                                "optional":true,
                                                "field":"generated"
                                            },
                                            {
                                                "type":"string",
                                                "optional":true,
                                                "field":"comment"
                                            }
                                        ],
                                        "optional":false,
                                        "name":"io.debezium.connector.schema.Column"
                                    },
                                    "optional":false,
                                    "field":"columns"
                                },
                                {
                                    "type":"string",
                                    "optional":true,
                                    "field":"comment"
                                }
                            ],
                            "optional":false,
                            "name":"io.debezium.connector.schema.Table",
                            "field":"table"
                        }
                    ],
                    "optional":false,
                    "name":"io.debezium.connector.schema.Change"
                },
                "optional":false,
                "field":"tableChanges"
            }
        ],
        "optional":false,
        "name":"io.debezium.connector.oracle.SchemaChangeValue"
    },
    "payload":{
        "source":{
            "version":"1.8.1.Final",
            "connector":"oracle",
            "name":"my-oracle-connector-0017",
            "ts_ms":1647175480000,
            "snapshot":"false",
            "db":"ORCLCDB",
            "sequence":null,
            "schema":"C##DDL_TEST",
            "table":"user",
            "txId":null,
            "scn":"16053472",
            "commit_scn":null,
            "lcr_position":null
        },
        "databaseName":"ORCLCDB",
        "schemaName":"C##DDL_TEST",
        "ddl":"CREATE TABLE \"C##DDL_TEST\".\"user\" (\n  \"id\" NUMBER NOT NULL,\n  \"name\" VARCHAR2(63) NOT NULL,\n\t\"role_id\" NUMBER,\n  PRIMARY KEY (\"id\"),\n  CONSTRAINT \"user_role_ref\" FOREIGN KEY (\"role_id\") REFERENCES \"C##DDL_TEST\".\"role\" (\"id\")\n);",
        "tableChanges":[
            {
                "type":"CREATE",
                "id":"\"ORCLCDB\".\"C##DDL_TEST\".\"user\"",
                "table":{
                    "defaultCharsetName":null,
                    "primaryKeyColumnNames":[
                        "id"
                    ],
                    "foreignKeyColumns":[
                        {
                            "pkColumnName":"id",
                            "fkName":"user_role_ref",
                            "fkColumnName":"role_id",
                            "pktableName":"role",
                            "pktableSchem":"C##DDL_TEST"
                        }
                    ],
                    "columns":[
                        {
                            "name":"id",
                            "jdbcType":2,
                            "nativeType":null,
                            "typeName":"NUMBER",
                            "typeExpression":"NUMBER",
                            "charsetName":null,
                            "length":38,
                            "scale":null,
                            "position":1,
                            "optional":false,
                            "autoIncremented":false,
                            "generated":false,
                            "comment":null
                        },
                        {
                            "name":"name",
                            "jdbcType":12,
                            "nativeType":null,
                            "typeName":"VARCHAR2",
                            "typeExpression":"VARCHAR2",
                            "charsetName":null,
                            "length":63,
                            "scale":null,
                            "position":2,
                            "optional":false,
                            "autoIncremented":false,
                            "generated":false,
                            "comment":null
                        },
                        {
                            "name":"role_id",
                            "jdbcType":2,
                            "nativeType":null,
                            "typeName":"NUMBER",
                            "typeExpression":"NUMBER",
                            "charsetName":null,
                            "length":38,
                            "scale":null,
                            "position":3,
                            "optional":true,
                            "autoIncremented":false,
                            "generated":false,
                            "comment":null
                        }
                    ],
                    "comment":null
                }
            }
        ]
    }
}
```


## 4.5子系统详细设计

| debezium | 初始化时的表结构数据提取和数据改变时间部分的外键分词提取，数据结构化等 |
| --- | --- |
| onlineMigration | DDL数据接收与转化，其中创建表部分紧实现了基本功能（创建表需求完成是合并，此处仅保障外键部分执行），外键语句转化 |


## 4.6DFX属性设计

### 4.6.1性能设计

debezium和onlineMigration的修改均对原有流程未做改变，不影响原有性能指标

### 4.6.2升级与扩容设计

_特性不会影响到升级和扩容_

### 4.6.3异常处理设计

_关注kafka connectet连接系的serviceid在每次启动时应保证不重复，都在有可能会发生快照过期_

### 4.6.4资源管理相关设计

_没有占用额外的内存、磁盘 **I/O** 、网络 **I/O** 等资源_

### 4.6.5小型化设计

_请说明特性不会影响小型化版本的规格（内存使用、安装包大小、 **CPU** 占用等）_

### 4.6.6可测性设计

_特性具备可测试性，详见4.8测试用例_

### 4.6.7安全设计

暂不会造成新的安全问题

## 4.7系统外部接口

_系统外部口未发送改变_

## 4.8自测用例设计

- 创建表时创建外键同步

| 用例编号 | 001 |
| --- | --- |
| 测试名称 | 创建表时创建外键同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br>2、oracle客户端查看与执行工具<br>3、kafka客户端工具：kafkatool<br>4、opengauss客户端查看工具<br>5、oracle创建对应的用户与授权<br>6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行创建表，创建两张表，其中包含外键关系<br><br>CREATE TABLE "C##DDL_TEST"."role" (  "id" NUMBER NOT NULL,<br>  "name" VARCHAR2(63) NOT NULL,<br>  PRIMARY KEY ("id")<br>);<br><br>CREATE TABLE "C##DDL_TEST"."user" (<br>  "id" NUMBER NOT NULL,<br>  "name" VARCHAR2(63) NOT NULL,<br>	"role_id" NUMBER,<br>  PRIMARY KEY ("id"),<br>  CONSTRAINT "user_role_ref" FOREIGN KEY ("role_id") REFERENCES "C##DDL_TEST"."role" ("id")<br>);<br><br>2、kafka接收到解析后解析外键正常，和oracle相同<br>3、opengauss同步创建表外键正确 |
| 预期结果 | opengauss中表的外键与oracle相同 |
| 实际结果 | PASS |


- 删除外键同步

| 用例编号 | 002 |
| --- | --- |
| 测试名称 | 删除外键同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br>2、oracle客户端查看与执行工具<br>3、kafka客户端工具：kafkatool<br>4、opengauss客户端查看工具<br>5、oracle创建对应的用户与授权<br>6、opengauss创建对应的用户和授权  |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br><br>ALTER TABLE "C##DDL_TEST"."user" DROP CONSTRAINT "user_role_ref"<br><br>2、kafka接收到解析后的删除语句，外键和oracle相同<br>3、opengauss同步创建表外键正确 |
| 预期结果 | opengauss中表的外键与oracle相同 |
| 实际结果 | PASS |


- 添加外键同步

| 用例编号 | 003 |
| --- | --- |
| 测试名称 | 添加外键同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br>2、oracle客户端查看与执行工具<br>3、kafka客户端工具：kafkatool<br>4、opengauss客户端查看工具<br>5、oracle创建对应的用户与授权<br>6、opengauss创建对应的用户和授权  |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br><br>ALTER TABLE "C##DDL_TEST"."user" ADD CONSTRAINT "user_role_ref" FOREIGN KEY ("role_id") REFERENCES "C##DDL_TEST"."role" ("id")<br><br>2、kafka接收到解析后的创建语句，外键和oracle相同<br>3、opengauss同步创建表外键正确 |
| 预期结果 | opengauss中表的外键与oracle相同 |
| 实际结果 | PASS |


·         **多外键同步**

| 用例编号 | 004 |
| --- | --- |
| 测试名称 | 多外键同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br>2、oracle客户端查看与执行工具<br>3、kafka客户端工具：kafkatool<br>4、opengauss客户端查看工具<br>5、oracle创建对应的用户与授权<br>6、opengauss创建对应的用户和授权  |
| 测试步骤 | 1、执行sql<br>CREATE TABLE "C##TEST"."user_fk" (<br>"id" NUMBER NOT NULL,<br>"name" VARCHAR2(63) NOT NULL,<br>"role_name" VARCHAR2(63),<br>"role_id" NUMBER,<br>PRIMARY KEY ("id"),<br>CONSTRAINT "user_fk_role_ref_id" FOREIGN KEY ("role_id") REFERENCES "C##TEST"."role" ("id"),<br>CONSTRAINT "user_fk_role_ref_name" FOREIGN KEY ("role_name") REFERENCES "C##TEST"."role" ("name")); |
<br>| 预期结果 | opengauss中表的外键与oracle相同 |
| 实际结果 | PASS |

·         默认不写**schema**外键同步

| 用例编号 | 005 |
| --- | --- |
| 测试名称 | 默认不写schema外键同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 |  1、测试环境已经就位，一个以上的测试语句能流程贯通<br>2、oracle客户端查看与执行工具<br>3、kafka客户端工具：kafkatool<br>4、opengauss客户端查看工具<br>5、oracle创建对应的用户与授权<br>6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、执行sql<br>CREATE TABLE "user_no_schema" (<br>"id" NUMBER NOT NULL,<br>"name" VARCHAR2(63) NOT NULL,<br>"role_name" VARCHAR2(63),<br>"role_id" NUMBER,<br>PRIMARY KEY ("id"),<br>CONSTRAINT "user_no_schema_role_ref_id" FOREIGN KEY ("role_id") REFERENCES "role" ("id"),<br>CONSTRAINT "user_no_schema_role_ref_name" FOREIGN KEY ("role_name") REFERENCES "role" ("name")<br>);|
| 预期结果 | opengauss中表的外键与oracle相同 |
| 实际结果 | PASS |

·         **外键生效验证**

| 用例编号 | 006 |
| --- | --- |
| 测试名称 | 外键生效验证 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 |  1、测试环境已经就位，一个以上的测试语句能流程贯通<br>2、oracle客户端查看与执行工具<br>3、kafka客户端工具：kafkatool<br>4、opengauss客户端查看工具<br>5、oracle创建对应的用户与授权<br>6、opengauss创建对应的用户和授权  |
| 测试步骤 | 1、执行sql<br>INSERT INTO "C##TEST"."role"("id", "name") VALUES ('1', 'admin');<br>INSERT INTO "C##TEST"."user"("id", "name", "role_id") VALUES ('1', 'test', '1');<br>INSERT INTO "C##TEST"."user"("id", "name", "role_id") VALUES ('1', 'test', '2'); |
| 预期结果 | 第1.2条sql执行成功，第3条sql因外键约束插入失败 |
| 实际结果 | PASS |


# 5.Use Case二实现

无

# 6.可靠性&可用性设计

_未改变原有系统设计。_

# 7.安全&隐私&韧性设计

_未改变原有系统设计。_

# 8.特性非功能性质量属性相关设计

## 8.1可测试性

- 关注测试环境搭建
- 安装oracle数据库，并按照debezium要求配置
- 依次安装zookeeper，kafka和kafka connector
- 到debezium下载客户端工具包
- 下载debezium 1.8.1-Final源码，合入paych包，打包完成后替换3中的debezium-core-1.8.1.Final.jar包和debezium-connector-oracle-1.8.1.Final.jar包
- 将4步中替换后的全量客户端包和oracle jdbc，xstream包分别加入到kafka lib库和connector的插件库
- 重启kafka connector
- 注册connector连接器，启动worker
- 安装opengauss数据库
- 打包openGauss-tools-onlineMigration，配置数据库和kafka参数后启动

## 8.2可服务性

- _关注数据库的可用行_
- _关注kafka connector worker的高可用_

## 8.3可演进性

_在系统原有架构上进行改变，涉及数据提取的部分公共层实现，涉及数据库特性的在本数据库的包中实现_

## 8.4开放性

接口的数据结构遵循原有debezium，接口的名称从jdbc规范文档保持一致，保障对外一致

## 8.5兼容性

_系统是在原有系统上增量提取数据，未对原有数据结构进行破坏，只是扩展新增，不影响原有版本使用。_

## 8.6可伸缩性/可扩展性

_与原有系统保持一致，未进行改变。_

## 8.7可维护性

参照原有系统，在关键节点打印日志，保障问题定位的快速和准确

## 8.8资料

_参考下表，评估特性会涉及到的各类资料的修改点，并说明具体修改点。_

| 类别 | 手册名称 | 是否涉及（Y/N) | 具体修改或新增内容简述 |
| --- | --- | --- | --- |
| 白皮书 | 技术白皮书 | N | 
 |
| 产品文档 | 产品描述 | Y | 新增外键特性 |
|  | 特性描述 | Y | 新增外键特性 |
|  | 编译指导书 | Y | 新增外键特性 |
|  | 安装指南 | N | 
 |
|  | 管理员指南 | N | 
 |
|  | 开发者指南 （包括开发教程、SQL参考、系统表和系统视图、GUC参数说明、错误码说明、API参考等） | N | 
 |
|  | 工具参考 | N | 
 |
|  | 术语表 | N | 
 |
| 入门 | 简易教程 | N | 
 |


# 9.数据结构设计（可选）

_不涉及_

# 10.参考资料清单
| **类型** | **备注** |
| --- | --- |
| 在线迁移支持**CREATE 外键 ** | https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/constraint.html#GUID-1055EA97-BA6F-4764-A15F-1024FD5B6DFE |
| 在线迁移支持**DROP 外键** | https://docs.oracle.com/cd/B19306_01/server.102/b14200/clauses002.htm |
| JAVA API DatabaseMetaData节选 | https://nowjava.com/docs/java-api-11/java.sql/java/sql/DatabaseMetaData.html |


