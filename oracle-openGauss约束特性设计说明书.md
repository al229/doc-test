**oracle-openGauss外**键**特性设计说明书**

| 所属SIG组: | 无 |
| --- | --- |
| 落入版本: | master |
| 设计人员: | 安磊 |
| 日期: | 2022-03-24 |


**Copyright © 2022 openGauss Community**

您对"本文档"的复制，使用，修改及分发受知识共享(Creative Commons)署名—相同方式共享4.0国际公共许可协议(以下简称"CC BY-SA 4.0")的约束。<br />为了方便用户理解，您可以通过访问[https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)了解CC BY-SA 4.0的概要 (但不是替代)。<br />CC BY-SA 4.0的完整协议内容您可以访问如下网址获取：[https://creativecommons.org/licenses/by-sa/4.0/legalcode](https://creativecommons.org/licenses/by-sa/4.0/legalcode)。

**改版记录**

| 日期 | 修订版本 | 修订描述 | 作者 | 审核 |
| --- | --- | --- | --- | --- |
| 2022-03-24 | 1.0 | 初始化 | 安磊 | 安磊 |
| <br /> | <br /> | <br /> | <br /> | <br /> |


<a name="0cd9e514"></a>
# 1.特性概述

_简述本产品 **/** 特性背景信息，简单概括本方案，对客户的价值、要达成的目的，并说明本文档包含的主要内容、适用范围等。_

<a name="115f0cff"></a>
## 1.1范围

基于openGauss开源项目onlineMigration（https://gitee.com/opengauss/openGauss-tools-onlineMigration），工具的在线迁移过程中需支持DDL语句迁移，即在线迁移阶段，Oracle侧的DDL语句能够同步到openGauss侧并成功执行

<a name="f8997477"></a>
## 1.2特性需求列表

| **类型** | **备注** |
| --- | --- |
| 在线迁移支持 CREATE约束 | https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/constraint.html#GUID-1055EA97-BA6F-4764-A15F-1024FD5B6DFE |
| 在线迁移支持 DROP约束 | https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/constraint.html#GUID-1055EA97-BA6F-4764-A15F-1024FD5B6DFE |
| 在线迁移支持 ALTER约束 | https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/constraint.html#GUID-1055EA97-BA6F-4764-A15F-1024FD5B6DFE |


<a name="0652c77c"></a>
# 2.需求场景分析

<a name="f7860faf"></a>
## 2.1特性需求来源与价值概述

**来源于客户需求**

- 项目：openGauss-Oracle在线迁移支持在线迁移DDL语句开发项目
- 功能需求：Oracle在线迁移工具支持在线迁移ADD/DROP/ALTER约束语法

<a name="e3285543"></a>
## 2.2特性场景分析


如果您需要从oracle向opengrauss数据库同步获迁移数据时，你的表中包含约束，则需要使用此特性

<a name="ba30023c"></a>
## 2.3特性影响分析

_约束是数据库常用的用法，在创建业务场景数据模型时，往往对一些字段要进行约束，例如主键可以提高查询效率，用户名唯一，默认值，不为null以及check条件等约束_

_与其他需求及特性的交互分析：该特性严重依赖创建表数据同步，尤其在_onlineMigration中

_平台差异性分析：使用java实现，可以实现跨平台使用_

_兼容性分析：对以前版本无影响，为改变数据标准结构，只是扩展字段_

_约束及限制：仅支持oracle到_opengrauss

<a name="f3d40286"></a>
### 2.3.1硬件限制

_在满足原有系统的硬件条件下，该特性并不会额外增加硬件消耗。_

<a name="0f6cd8cb"></a>
### 2.3.2技术限制

_操作系统：跨平台_

_编程语言：java_


<a name="b4493a67"></a>
### 2.3.3对License的影响分析

_无新增三方依赖。_

<a name="c4b185ac"></a>
### 2.3.4对系统性能规格的影响分析

无新增系统消耗

<a name="6e3aedf1"></a>
### 2.3.5对系统可靠性规格的影响分析

不对原有系统可靠性行造成影响

<a name="5040e892"></a>
### 2.3.6对系统兼容性的影响分析

不对原有系统兼容性行造成影响

<a name="63e345cd"></a>
### 2.3.7与其他重大特性的交互性，冲突性的影响分析

_不改变原有系统交互_

<a name="8e374a67"></a>
## 2.4同类社区/商用软件实现方案分析

暂无

<a name="d66b9251"></a>
# 3.特性/功能实现原理(可分解出来多个Use Case)

<a name="1be76950"></a>
## 3.1目标

工具的在线迁移阶段，在ORACLE侧执行CREATE/DROP/Alter 约束语法能够同步到openGauss侧并成功执行

<a name="52f08a08"></a>
## 3.2总体方案

- _在debezium测对数据进行分析，将提取的约束放入到table对象，在加入change时间_
- _对change事件进行监控，获取数据，进行分析和结构化处理，发送到kafka_
- onlineMigration从kafka获取到结构化的消息，进行提取和转化，转成opengrauss的sql，执行到数据库

![Image text](https://gitee.com/al229/doc/raw/master/pic/3.2png.png)

<a name="655a3472"></a>
# 4.Use Case一实现

<a name="13ef2127"></a>
## 4.1设计思路

   - 原始sql监控和解析在debezium侧，按照现有开源方案标准格式输出
   - 标准格式数据转化成opengaruss sql 在onlineMigration执行

<a name="26212552"></a>
## 4.2约束条件

标准格式数据转化成opengaruss sql 余数部分依赖与创建表部分

<a name="d7a994e1"></a>
## 4.3详细实现(从用户入口的模块级别或进程级别消息序列图)

 ![Image text](https://gitee.com/al229/doc/raw/master/pic/%E6%95%B0%E6%8D%AE%E5%BA%93%E5%90%8C%E6%AD%A5%E6%B5%81%E7%A8%8B-debezium.jpg)


<a name="53ef879d"></a>
## 4.4子系统间接口(主要覆盖模块接口定义)

- 创建表的外键输入格式

| **字段** | **是否必须** | **描述** |
| --- | --- | --- |
| primaryKeyColumnNames | 否 | 主键名称 |
| uniqueColumns | 否 | 唯一约束列表，包含indexName唯一约束名称和columnName 列名 |
| checkColumns | 否 | 检查约束，包含condition检查条件和columnName 列名 |
| optional | 否 | true：可以未为null，<br />false：不能为null |
| defaultValueExpression | 否 | 默认值 |
| primaryKeyColumnChanges | 否 | 修改时主键是否发生变化，包含action，新增或删除，columnName 列名 |
| modifyKeys | 否 | 修改时发送改变的属性，包含defaultValueExpression和<br />optional |



- 新增对象
```json
"uniqueColumns": [
            {
              "indexName": "unique_login_name2",
              "columnName": "login_name"
            },
            {
              "indexName": "unique_name2",
              "columnName": "name"
            }
          ],
"checkColumns": [
            {
              "condition": "\"age\" BETWEEN 0 and 150",
              "indexName": "check_age2"
            },
            {
              "condition": "\"address\" in (\u0027china\u0027) AND \"address\" \u003d \u0027USA\u0027",
              "indexName": "check_address"
            }
          ]

"primaryKeyColumnChanges": [
            {
              "action": "ADD",
              "columnName": "id"
            }
          ]

{
              "name": "address",
              "jdbcType": 12,
              "nativeType": null,
              "typeName": "VARCHAR2",
              "typeExpression": "VARCHAR2",
              "charsetName": null,
              "length": 63,
              "scale": null,
              "position": 5,
              "optional": false,
              "defaultValueExpression": "NULL",
              "autoIncremented": false,
              "generated": false,
              "comment": null,
              "modifyKeys": [
                "defaultValueExpression",
                "optional"
              ]
            }
```

- 全量数据数据格式
```json
{
  "schema": {
    "type": "struct",
    "fields": [
      {
        "type": "struct",
        "fields": [
          {
            "type": "string",
            "optional": false,
            "field": "version"
          },
          {
            "type": "string",
            "optional": false,
            "field": "connector"
          },
          {
            "type": "string",
            "optional": false,
            "field": "name"
          },
          {
            "type": "int64",
            "optional": false,
            "field": "ts_ms"
          },
          {
            "type": "string",
            "optional": true,
            "name": "io.debezium.data.Enum",
            "version": 1,
            "parameters": {
              "allowed": "true,last,false,incremental"
            },
            "default": "false",
            "field": "snapshot"
          },
          {
            "type": "string",
            "optional": false,
            "field": "db"
          },
          {
            "type": "string",
            "optional": true,
            "field": "sequence"
          },
          {
            "type": "string",
            "optional": false,
            "field": "schema"
          },
          {
            "type": "string",
            "optional": false,
            "field": "table"
          },
          {
            "type": "string",
            "optional": true,
            "field": "txId"
          },
          {
            "type": "string",
            "optional": true,
            "field": "scn"
          },
          {
            "type": "string",
            "optional": true,
            "field": "commit_scn"
          },
          {
            "type": "string",
            "optional": true,
            "field": "lcr_position"
          }
        ],
        "optional": false,
        "name": "io.debezium.connector.oracle.Source",
        "field": "source"
      },
      {
        "type": "string",
        "optional": true,
        "field": "databaseName"
      },
      {
        "type": "string",
        "optional": true,
        "field": "schemaName"
      },
      {
        "type": "string",
        "optional": true,
        "field": "ddl"
      },
      {
        "type": "array",
        "items": {
          "type": "struct",
          "fields": [
            {
              "type": "string",
              "optional": false,
              "field": "type"
            },
            {
              "type": "string",
              "optional": false,
              "field": "id"
            },
            {
              "type": "struct",
              "fields": [
                {
                  "type": "string",
                  "optional": true,
                  "field": "defaultCharsetName"
                },
                {
                  "type": "array",
                  "items": {
                    "type": "string",
                    "optional": false
                  },
                  "optional": true,
                  "field": "primaryKeyColumnNames"
                },
                {
                  "type": "array",
                  "items": {
                    "type": "map",
                    "keys": {
                      "type": "string",
                      "optional": true
                    },
                    "values": {
                      "type": "string",
                      "optional": true
                    },
                    "optional": true
                  },
                  "optional": true,
                  "field": "primaryKeyColumnChanges"
                },
                {
                  "type": "array",
                  "items": {
                    "type": "map",
                    "keys": {
                      "type": "string",
                      "optional": true
                    },
                    "values": {
                      "type": "string",
                      "optional": true
                    },
                    "optional": true
                  },
                  "optional": true,
                  "field": "foreignKeyColumns"
                },
                {
                  "type": "array",
                  "items": {
                    "type": "map",
                    "keys": {
                      "type": "string",
                      "optional": true
                    },
                    "values": {
                      "type": "string",
                      "optional": true
                    },
                    "optional": true
                  },
                  "optional": true,
                  "field": "uniqueColumns"
                },
                {
                  "type": "array",
                  "items": {
                    "type": "map",
                    "keys": {
                      "type": "string",
                      "optional": true
                    },
                    "values": {
                      "type": "string",
                      "optional": true
                    },
                    "optional": true
                  },
                  "optional": true,
                  "field": "checkColumns"
                },
                {
                  "type": "array",
                  "items": {
                    "type": "struct",
                    "fields": [
                      {
                        "type": "string",
                        "optional": false,
                        "field": "name"
                      },
                      {
                        "type": "int32",
                        "optional": false,
                        "field": "jdbcType"
                      },
                      {
                        "type": "int32",
                        "optional": true,
                        "field": "nativeType"
                      },
                      {
                        "type": "string",
                        "optional": false,
                        "field": "typeName"
                      },
                      {
                        "type": "string",
                        "optional": true,
                        "field": "typeExpression"
                      },
                      {
                        "type": "string",
                        "optional": true,
                        "field": "charsetName"
                      },
                      {
                        "type": "int32",
                        "optional": true,
                        "field": "length"
                      },
                      {
                        "type": "int32",
                        "optional": true,
                        "field": "scale"
                      },
                      {
                        "type": "int32",
                        "optional": false,
                        "field": "position"
                      },
                      {
                        "type": "boolean",
                        "optional": true,
                        "field": "optional"
                      },
                      {
                        "type": "string",
                        "optional": true,
                        "field": "defaultValueExpression"
                      },
                      {
                        "type": "boolean",
                        "optional": true,
                        "field": "autoIncremented"
                      },
                      {
                        "type": "boolean",
                        "optional": true,
                        "field": "generated"
                      },
                      {
                        "type": "string",
                        "optional": true,
                        "field": "comment"
                      },
                      {
                        "type": "array",
                        "items": {
                          "type": "string",
                          "optional": false
                        },
                        "optional": true,
                        "field": "modifyKeys"
                      }
                    ],
                    "optional": false,
                    "name": "io.debezium.connector.schema.Column"
                  },
                  "optional": false,
                  "field": "columns"
                },
                {
                  "type": "string",
                  "optional": true,
                  "field": "comment"
                }
              ],
              "optional": false,
              "name": "io.debezium.connector.schema.Table",
              "field": "table"
            }
          ],
          "optional": false,
          "name": "io.debezium.connector.schema.Change"
        },
        "optional": false,
        "field": "tableChanges"
      }
    ],
    "optional": false,
    "name": "io.debezium.connector.oracle.SchemaChangeValue"
  },
  "payload": {
    "source": {
      "version": "1.8.1.Final",
      "connector": "oracle",
      "name": "my-oracle-connector-0025",
      "ts_ms": 1648049972000,
      "snapshot": "false",
      "db": "ORCLCDB",
      "sequence": null,
      "schema": "C##TEST",
      "table": "user_info",
      "txId": null,
      "scn": "28580702",
      "commit_scn": null,
      "lcr_position": null
    },
    "databaseName": "ORCLCDB",
    "schemaName": "C##TEST",
    "ddl": "CREATE TABLE \"C##TEST\".\"user_info\" (\n  \"id\" NUMBER NOT NULL,\n  \"name\" VARCHAR2(63) NOT NULL,\n \"login_name\" VARCHAR2(63) NOT NULL,\n  \"age\" NUMBER DEFAULT 18,\n  \"address\" VARCHAR2(63) DEFAULT \u0027china\u0027,\n  PRIMARY KEY (\"id\"),\n  CONSTRAINT \"unique_name\" UNIQUE (\"name\"),\n\tCONSTRAINT \"unique_login_name\" UNIQUE (\"login_name\"),\n  CONSTRAINT \"check_age\" CHECK (\"age\" BETWEEN 0 and 150),\n\tCONSTRAINT \"check_address\" CHECK (\"address\" in (\u0027china\u0027) AND \"address\" \u003d \u0027USA\u0027)\n);",
    "tableChanges": [
      {
        "type": "CREATE",
        "id": "\"ORCLCDB\".\"C##TEST\".\"user_info\"",
        "table": {
          "defaultCharsetName": null,
          "primaryKeyColumnNames": [
            "id"
          ],
          "primaryKeyColumnChanges": null,
          "foreignKeyColumns": [],
          "uniqueColumns": [
            {
              "indexName": "unique_name",
              "columnName": "name"
            },
            {
              "indexName": "unique_login_name",
              "columnName": "login_name"
            }
          ],
          "checkColumns": [
            {
              "condition": "\"age\" BETWEEN 0 and 150",
              "indexName": "check_age"
            },
            {
              "condition": "\"address\" in (\u0027china\u0027) AND \"address\" \u003d \u0027USA\u0027",
              "indexName": "check_address"
            }
          ],
          "columns": [
            {
              "name": "id",
              "jdbcType": 2,
              "nativeType": null,
              "typeName": "NUMBER",
              "typeExpression": "NUMBER",
              "charsetName": null,
              "length": 38,
              "scale": null,
              "position": 1,
              "optional": false,
              "defaultValueExpression": null,
              "autoIncremented": false,
              "generated": false,
              "comment": null,
              "modifyKeys": null
            },
            {
              "name": "name",
              "jdbcType": 12,
              "nativeType": null,
              "typeName": "VARCHAR2",
              "typeExpression": "VARCHAR2",
              "charsetName": null,
              "length": 63,
              "scale": null,
              "position": 2,
              "optional": false,
              "defaultValueExpression": null,
              "autoIncremented": false,
              "generated": false,
              "comment": null,
              "modifyKeys": null
            },
            {
              "name": "login_name",
              "jdbcType": 12,
              "nativeType": null,
              "typeName": "VARCHAR2",
              "typeExpression": "VARCHAR2",
              "charsetName": null,
              "length": 63,
              "scale": null,
              "position": 3,
              "optional": false,
              "defaultValueExpression": null,
              "autoIncremented": false,
              "generated": false,
              "comment": null,
              "modifyKeys": null
            },
            {
              "name": "age",
              "jdbcType": 2,
              "nativeType": null,
              "typeName": "NUMBER",
              "typeExpression": "NUMBER",
              "charsetName": null,
              "length": 38,
              "scale": null,
              "position": 4,
              "optional": true,
              "defaultValueExpression": "18",
              "autoIncremented": false,
              "generated": false,
              "comment": null,
              "modifyKeys": null
            },
            {
              "name": "address",
              "jdbcType": 12,
              "nativeType": null,
              "typeName": "VARCHAR2",
              "typeExpression": "VARCHAR2",
              "charsetName": null,
              "length": 63,
              "scale": null,
              "position": 5,
              "optional": true,
              "defaultValueExpression": "\u0027china\u0027",
              "autoIncremented": false,
              "generated": false,
              "comment": null,
              "modifyKeys": null
            }
          ],
          "comment": null
        }
      }
    ]
  }
}
```


<a name="052fbc54"></a>
## 4.5子系统详细设计

| debezium | 初始化时的表结构数据提取和数据改变时间部分的外键分词提取，数据结构化等 |
| --- | --- |
| onlineMigration | DDL数据接收与转化，其中创建表部分紧实现了基本功能（创建表需求完成是合并，此处仅保障外键部分执行），外键语句转化 |


<a name="16629dc7"></a>
## 4.6DFX属性设计

<a name="077f6235"></a>
### 4.6.1性能设计

debezium和onlineMigration的修改均对原有流程未做改变，不影响原有性能指标

<a name="91aa30f8"></a>
### 4.6.2升级与扩容设计

_特性不会影响到升级和扩容_

<a name="c6597c23"></a>
### 4.6.3异常处理设计

_关注kafka connectet连接系的serviceid在每次启动时应保证不重复，都在有可能会发生快照过期_

<a name="6aa3d250"></a>
### 4.6.4资源管理相关设计

_没有占用额外的内存、磁盘 **I/O** 、网络 **I/O** 等资源_

<a name="2212bf1b"></a>
### 4.6.5小型化设计

_请说明特性不会影响小型化版本的规格（内存使用、安装包大小、 **CPU** 占用等）_

<a name="42b467c6"></a>
### 4.6.6可测性设计

_特性具备可测试性，详见4.8测试用例_

<a name="b643be36"></a>
### 4.6.7安全设计

暂不会造成新的安全问题

<a name="5b97c3a3"></a>
## 4.7系统外部接口

_系统外部口未发送改变_

<a name="8d180303"></a>
## 4.8自测用例设计

（1）创建表时创建约束同步

| 用例编号 | 001 |
| --- | --- |
| 测试名称 | 创建表时创建约束同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行创建表，创建两张表，其中包含外键关系<br />CREATE TABLE "C##TEST"."user_info" (<br />  "id" NUMBER NOT NULL,<br />  "name" VARCHAR2(63) NOT NULL,<br /> "login_name" VARCHAR2(63) NOT NULL,<br />  "age" NUMBER DEFAULT 18,<br />  "address" VARCHAR2(63) DEFAULT 'china',<br />  PRIMARY KEY ("id"),<br />  CONSTRAINT "unique_name" UNIQUE ("name"),<br />	CONSTRAINT "unique_login_name" UNIQUE ("login_name"),<br />  CONSTRAINT "check_age" CHECK ("age" BETWEEN 0 and 150),<br />	CONSTRAINT "check_address" CHECK ("address" in ('china') AND "address" = 'USA')<br />);<br />2、kafka接收到解析后解析约束正常，和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |


（2） 删除主键同步

| 用例编号 | 002 |
| --- | --- |
| 测试名称 | 删除主键同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info2" DROP PRIMARY KEY<br />2、kafka接收到解析后的删除语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |


（3）添加主键同步

| 用例编号 | 003 |
| --- | --- |
| 测试名称 | 添加主键同步 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info2" ADD PRIMARY KEY ("id")<br />2、kafka接收到解析后的创建语句，主键和oracle相同<br />3、opengauss同步创建表主键正确 |
| 预期结果 | opengauss中表的主键与oracle相同 |
| 实际结果 | PASS |


（4）check-创建表时多类型，且无命名

| 用例编号 | 004 |
| --- | --- |
| 测试名称 | 创建表的check多类型，且没有命名 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />CREATE TABLE "C##TEST"."divisions"<br />   (div_no    NUMBER  CONSTRAINT check_divno<br />              CHECK (div_no BETWEEN 10 AND 99) <br />              DISABLE, <br />    div_name  VARCHAR2(9)  CONSTRAINT check_divname<br />              CHECK (div_name = UPPER(div_name)) <br />              DISABLE, <br />    office    VARCHAR2(10)  CONSTRAINT check_office<br />              CHECK (office IN ('DALLAS','BOSTON',<br />              'PARIS','TOKYO')) <br />              DISABLE);<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |



(5)check-创建表的check混合运算

| 用例编号 | 005 |
| --- | --- |
| 测试名称 | 创建表的check混合运算 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />CREATE TABLE "C##TEST"."dept_20"<br />   (employee_id     NUMBER(4) PRIMARY KEY, <br />    last_name       VARCHAR2(10), <br />    job_id          VARCHAR2(9), <br />    manager_id      NUMBER(4), <br />    salary          NUMBER(7,2), <br />    commission_pct  NUMBER(7,2), <br />    department_id   NUMBER(2),<br />    CONSTRAINT check_sal CHECK (salary / (commission_pct - commission_pct) <= 5000 - (commission_pct/30)));<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |



(6)check-创建表的check 多条件嵌套

| 用例编号 | 006 |
| --- | --- |
| 测试名称 | 创建表的check 多条件嵌套 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />CREATE TABLE "C##TEST"."user_info_test" (<br />  "id" NUMBER NOT NULL,<br />  "name" VARCHAR2(63) NOT NULL,<br /> "login_name" VARCHAR2(63) NOT NULL,<br />  "age" NUMBER DEFAULT 18,<br />  "address" VARCHAR2(63) DEFAULT 'china',<br />  PRIMARY KEY ("id"),<br />  CONSTRAINT "unique_name_test" UNIQUE ("name"),<br />	CONSTRAINT "unique_login_name_test" UNIQUE ("login_name"),<br />  CONSTRAINT "check_age_test" CHECK ("age" BETWEEN 0 and 150),<br />	CONSTRAINT "check_address_test" CHECK ("address" in ('china') AND ("address" LIKE '中国' OR "address" is NOT NULL) AND "address" != '日本')<br />);	<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |



(7)check-删除check

| 用例编号 | 007 |
| --- | --- |
| 测试名称 | 删除check |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info" DROP CONSTRAINT "check_age"<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |



(8)check-修改Check

| 用例编号 | 008 |
| --- | --- |
| 测试名称 | 修改check |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info_test" ADD CONSTRAINT "check_address_test_update"  CHECK ("address" in ('china') AND ("address" LIKE '中国' OR "address" is NOT NULL) AND ("address" != '日本' OR  "address" != '米国'));<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |



(9)修改默认值

| 用例编号 | 009 |
| --- | --- |
| 测试名称 | 修改默认值 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info" <br />MODIFY ("address"  DEFAULT '中国');<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |


(10)取消默认值

| 用例编号 | 010 |
| --- | --- |
| 测试名称 | 取消默认值 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info" <br />MODIFY ("address"  DEFAULT NULL)<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |


(11)check-设置字段NOT NULL

| 用例编号 | 011 |
| --- | --- |
| 测试名称 | 设置字段 NOT NULL |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info" <br />MODIFY ("address"  DEFAULT NULL NOT NULL)<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |

(12)设置字段可以为NULL

| 用例编号 | 012 |
| --- | --- |
| 测试名称 | 设置字段可以为NULL |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、在oracle，C##DDL_TEST用户下执行删除user表的外键<br />ALTER TABLE "C##TEST"."user_info" <br />MODIFY ("address"  NULL);<br />2、kafka接收到解析后的创建语句，约束和oracle相同<br />3、opengauss同步创建表约束正确 |
| 预期结果 | opengauss中表的约束与oracle相同 |
| 实际结果 | PASS |


(13)数据验证-主键

| 用例编号 | 013 |
| --- | --- |
| 测试名称 | 数据验证-主键 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、执行sql<br />INSERT INTO "C##TEST"."user_info_test"("id", "name", "login_name", "age", "address") VALUES (1, '测试', 'test', 18, 'china');<br />INSERT INTO "C##TEST"."user_info_test"("id", "name", "login_name", "age", "address") VALUES (1, '测试1', 'test1', 16, 'china');<br />|
| 预期结果 | 第一条sql成功插入，第二条sql因为主键约束无法插入 |
| 实际结果 | PASS |



(14)数据验证-check

| 用例编号 | 014 |
| --- | --- |
| 测试名称 | 数据验证-check |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权|
| 测试步骤 | 1、执行sql<br />INSERT INTO "C##TEST"."user_info_test"("id", "name", "login_name", "age", "address") VALUES (1, '测试1', 'test1', -1, 'china');<br />|
| 预期结果 | 年龄约束在1-150约束之外，因此插入约束校验插入失败 |
| 实际结果 | PASS |



(15)数据验证-默认值

| 用例编号 | 015 |
| --- | --- |
| 测试名称 | 数据验证-默认值 |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、执行sql<br />INSERT INTO "C##TEST"."user_info_test"("id", "name", "login_name", "age") VALUES (2, '测试default', 'test-default',16); <br />|
| 预期结果 | 地址默认值为china |
| 实际结果 | PASS |



(16)数据验证-默认值为NULL

| 用例编号 | 016 |
| --- | --- |
| 测试名称 | 数据验证-默认值为NULL |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1执行用例010<br />2.执行sql<br />INSERT INTO "C##TEST"."user_info_test"("id", "name", "login_name", "age") VALUES (3, '测试default-null', 'test-default-null',16);<br />|
| 预期结果 | 地址默认为NULL |
| 实际结果 | PASS |



(17)数据验证-NOT NULL

| 用例编号 | 017 |
| --- | --- |
| 测试名称 | 数据验证-NOT NULL |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权 |
| 测试步骤 | 1、执行sql<br />INSERT INTO "C##TEST"."user_info_test"("id", "name", "login_name", "age") VALUES (4, NULL, 'test-default-null',16);<br />|
| 预期结果 | NAME不能为null，插入失败 |
| 实际结果 | PASS |


(18) 数据验证-可以为NULL

| 用例编号 | 018 |
| --- | --- |
| 测试名称 | 数据验证-可以为null |
| 测试环境 | oracle->debezium->kafka->online->opengauss 同步环境 |
| 预置条件 | 1、测试环境已经就位，一个以上的测试语句能流程贯通<br />2、oracle客户端查看与执行工具<br />3、kafka客户端工具：kafkatool<br />4、opengauss客户端查看工具<br />5、oracle创建对应的用户与授权<br />6、opengauss创建对应的用户和授权|
| 测试步骤 | 1, 执行用例012<br />2、执行sql<br />INSERT INTO "C##TEST"."user_info_test"("id", "name", "login_name", "age", "address") VALUES (1, '测试1', 'test1', -1, NULL);  <br />|
| 预期结果 | 地址可以为NULL，执行成功 |
| 实际结果 | PASS |



<a name="e226e88b"></a>
# 5.Use Case二实现

无

<a name="0fe0415b"></a>
# 6.可靠性&可用性设计

_未改变原有系统设计。_

<a name="add7346a"></a>
# 7.安全&隐私&韧性设计

_未改变原有系统设计。_

<a name="d3c8bb34"></a>
# 8.特性非功能性质量属性相关设计

<a name="fdf82007"></a>
## 8.1可测试性

- 关注测试环境搭建
- 安装oracle数据库，并按照debezium要求配置
- 依次安装zookeeper，kafka和kafka connector
- 到debezium下载客户端工具包
- 下载debezium 1.8.1-Final源码，合入paych包，打包完成后替换3中的debezium-core-1.8.1.Final.jar包和debezium-connector-oracle-1.8.1.Final.jar包
- 将4步中替换后的全量客户端包和oracle jdbc，xstream包分别加入到kafka lib库和connector的插件库
- 重启kafka connector
- 注册connector连接器，启动worker
- 安装opengauss数据库
- 打包openGauss-tools-onlineMigration，配置数据库和kafka参数后启动

<a name="8120a3fc"></a>
## 8.2可服务性

- _关注数据库的可用行_
- _关注kafka connector worker的高可用_

<a name="ae0a35ee"></a>
## 8.3可演进性

_在系统原有架构上进行改变，涉及数据提取的部分公共层实现，涉及数据库特性的在本数据库的包中实现_

<a name="9823389a"></a>
## 8.4开放性

接口的数据结构遵循原有debezium，接口的名称从jdbc规范文档保持一致，保障对外一致

<a name="25d2fb33"></a>
## 8.5兼容性

_系统是在原有系统上增量提取数据，未对原有数据结构进行破坏，只是扩展新增，不影响原有版本使用。_

<a name="f48a0b3a"></a>
## 8.6可伸缩性/可扩展性

_与原有系统保持一致，未进行改变。_

<a name="4dbe97d0"></a>
## 8.7可维护性

参照原有系统，在关键节点打印日志，保障问题定位的快速和准确

<a name="5b26a266"></a>
## 8.8资料

_参考下表，评估特性会涉及到的各类资料的修改点，并说明具体修改点。_

| 类别 | 手册名称 | 是否涉及（Y/N) | 具体修改或新增内容简述 |
| --- | --- | --- | --- |
| 白皮书 | 技术白皮书 | N | <br /> |
| 产品文档 | 产品描述 | Y | 新增外键特性 |
|  | 特性描述 | Y | 新增外键特性 |
|  | 编译指导书 | Y | 新增外键特性 |
|  | 安装指南 | N | <br /> |
|  | 管理员指南 | N | <br /> |
|  | 开发者指南 （包括开发教程、SQL参考、系统表和系统视图、GUC参数说明、错误码说明、API参考等） | N | <br /> |
|  | 工具参考 | N | <br /> |
|  | 术语表 | N | <br /> |
| 入门 | 简易教程 | N | <br /> |


<a name="1ec7a9e1"></a>
# 9.数据结构设计（可选）

_不涉及_

<a name="fd0ba3f5"></a>
# 10.参考资料清单
| **类型** | **备注** |
| --- | --- |
| 在线迁移支持 | https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/constraint.html#GUID-1055EA97-BA6F-4764-A15F-1024FD5B6DFE |
| JAVA API DatabaseMetaData节选 | https://nowjava.com/docs/java-api-11/java.sql/java/sql/DatabaseMetaData.html |


